﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MoviesDomain
{
    /// <summary>
    /// Предоставляет интерфейс к источнику фильмов, как, например, TMDB
    /// </summary>
    public interface IMovieProvider : IDisposable
    {
        /// <summary>
        /// Получить список лучших фильмов
        /// </summary>
        /// <param name="numberOfMovies">Количество возвращаемых фильмов</param>
        /// <param name="token">Токен отмены</param>
        /// <returns></returns>
        Task<List<IMovie>> GetHighestRatedMovies(int numberOfMovies, CancellationToken token = default(CancellationToken));

        /// <summary>
        /// Поиск фильма по неполному названию
        /// </summary>
        /// <param name="name">Название</param>
        /// <returns></returns>
        Task<List<IMovie>> SearchMovieLike(string name, CancellationToken token = default(CancellationToken));
    }
}
