﻿using System;
using System.Collections.Generic;

namespace MoviesDomain
{
    /// <summary>
    /// Интерфейс для фильма.
    /// </summary>
    public interface IMovie : IEquatable<IMovie>
    {
        /// <summary>
        /// Идентификатор фильма во внутренней БД
        /// </summary>
        string MovieId { set; get; }

        /// <summary>
        /// Название фильма
        /// </summary>
        string Title { set;  get; }
        
        /// <summary>
        /// Путь к постеру 
        /// </summary>
        Uri PosterPath { set;  get; }
        
        /// <summary>
        /// Дата выхода
        /// </summary>
        DateTime ReleaseDate { set;  get; }

        /// <summary>
        /// Тэги
        /// </summary>
        List<string> Tags { set;  get; }

        /// <summary>
        /// Описание фильма
        /// </summary>
        MovieDescription Description { set; get; }
    }
}