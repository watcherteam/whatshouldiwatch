﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesDomain
{
    /// <summary>
    /// Класс записи кэша
    /// </summary>
    class MovieCacheEntry
    {
        /// <summary>
        /// Фильм
        /// </summary>
        public IMovie Movie { set; get; }

        /// <summary>
        /// Дата последнего обращения
        /// </summary>
        public DateTime Timestamp { set; get; }

        /// <summary>
        /// Id фильма
        /// </summary>
        public string MovieId
        {
            get
            {
                return Movie.MovieId;
            }
        }
    }

    /// <summary>
    /// Класс кэша фильмов
    /// </summary>
    public class MovieCache
    {
        /// <summary>
        /// Создание нового кэша размера size
        /// </summary>
        /// <param name="size"></param>
        public MovieCache(int size)
        {
            this.size = size;
            cache = new Dictionary<string, IMovie>(size);
            timestamps = new Dictionary<string, DateTime>(size);
        }

        /// <summary>
        /// Попытаться извлечь из кэша фильм
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns>Фильм (если не найден, то null)</returns>
        public IMovie GetMovie(string movieId)
        {
            if (cache.ContainsKey(movieId))
            {
                timestamps[movieId] = DateTime.Now;
                return cache[movieId];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Добавить в кэш фильм (если уже есть, то обновляет временную метку)
        /// </summary>
        /// <param name="movie"></param>
        public void AddMovie(IMovie movie)
        {
            if (cache.ContainsKey(movie.MovieId))
            {
                timestamps[movie.MovieId] = DateTime.Now;
            }
            else
            {
                if (cache.Count >= size)
                {
                    var movieToDelete = timestamps.First(x => x.Value == timestamps.Min(m => m.Value)).Key;
                    cache.Remove(movieToDelete);
                    timestamps.Remove(movieToDelete);
                }
                cache.Add(movie.MovieId, movie);
                timestamps.Add(movie.MovieId, DateTime.Now);
            }
        }

        /// <summary>
        /// Словарь записей вида (id, фильм)
        /// </summary>
        Dictionary<string, IMovie> cache;
        
        /// <summary>
        /// Словарь записей вида (id, дата последнего обращения)
        /// </summary>
        Dictionary<string, DateTime> timestamps;

        /// <summary>
        /// Размер кэша
        /// </summary>
        private int size;
    }
}
