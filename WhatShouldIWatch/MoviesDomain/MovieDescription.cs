﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MoviesDomain
{
    public class MovieDescription : IEquatable<MovieDescription>
    {
        public MovieDescription() { }

        public MovieDescription(string descriptionText, IEnumerable<string> genre, string director, IEnumerable<string> actors)
        {
            if (descriptionText == null) throw new ArgumentNullException(nameof(descriptionText));
            if (genre == null) throw new ArgumentNullException(nameof(genre));
            if (director == null) throw new ArgumentNullException(nameof(director));
            if (actors == null) throw new ArgumentNullException(nameof(actors));

            this.DescriptionText = descriptionText;
            this.Genre = genre.ToList();
            this.Director = director;
            this.Actors = actors.ToList();
        }

        public bool Equals(MovieDescription other)
        {
            if (other == null)
            {
                return false;
            }

            return DescriptionText == other.DescriptionText
                   && Genre.SequenceEqual(other.Genre)
                   && Director == other.Director
                   && Actors.SequenceEqual(other.Actors);
        }
        
        private Guid guid;

        public string MovieDescriptionId
        {
            set
            {
                guid = Guid.Parse(value);
            }
            get
            {
                if (guid == Guid.Empty)
                {
                    guid = Guid.NewGuid();
                }
                return guid.ToString();
            }
        }

        public string DescriptionText { set; get; }
        
        public string Director { set; get; }
        
        public List<string> Genre { set; get; }
        
        public List<string> Actors { set; get; }
    }
}
