﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MoviesDomain
{
    public class SimilarityRecommendation : RecommendationAlgorithm
    {
        public List<IMovie> MakeRecommendation(IMovie original, IEnumerable<IMovie> allMovies, int numberOfRecommendations)
        {
            return allMovies
                .Select(movie => new {Score = ComputeScore(original, movie), Movie = movie })
                .OrderByDescending(x => x.Score)
                .Take(numberOfRecommendations)
                .Select(x => x.Movie)
                .ToList();
        }

        /// <summary>
        /// Подсчитывает, насколько фильм b похож на фильм a
        /// </summary>
        /// <returns>Счёт похожести: |a.tags ∩ b.tags|/|a.tags|</returns>
        private double ComputeScore(IMovie a, IMovie b)
        {
            if (a.Tags.Count == 0)
            {
                return 1;
            }

            return a.Tags.Intersect(b.Tags).Count() / (double)a.Tags.Count;
        }
    }
}