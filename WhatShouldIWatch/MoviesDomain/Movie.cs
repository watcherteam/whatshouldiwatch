﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MoviesDomain
{
    public class Movie : IMovie
    {
        public Movie() { }

        public Movie(string title, MovieDescription description, IEnumerable<string> tags, DateTime releaseDate, Uri posterPath)
        {
            if (string.IsNullOrEmpty(title)) throw new ArgumentException(nameof(title));
            if (description == null) throw new ArgumentNullException(nameof(description));
            if (tags == null) throw new ArgumentNullException(nameof(tags));
            
            this.Title = title;
            this.Description = description;
            this.Tags = tags.ToList();
            this.ReleaseDate = releaseDate;
            this.PosterPath = posterPath;
        }

        public bool Equals(IMovie other)
        {
            if (other == null)
            {
                return false;
            }

            var tmp = Description.Equals(other.Description);
            tmp = tmp && Tags.SequenceEqual(other.Tags);
            tmp = tmp && ReleaseDate.Equals(other.ReleaseDate);
            return tmp;
        }

        private Guid guid;

        public string MovieId
        {
            set
            {
                guid = Guid.Parse(value);
            }
            get
            {
                if (guid == Guid.Empty)
                {
                    guid = Guid.NewGuid();
                }
                return guid.ToString();
            }
        }
        
        public string TitleYear => Title + " (" + ReleaseDate.Year + ")";

        public string Title { set; get; }

        public MovieDescription Description { set; get; }

        public List<string> Tags { set; get; }

        private DateTime releaseDate;

        public DateTime ReleaseDate
        {
            set
            {
                // обнуляем миллисекунды для корректного сохранения в БД ()
                releaseDate = new DateTime(value.Year, value.Month, value.Day, value.Hour, value.Minute, value.Second);
            }
            get
            {
                return releaseDate;
            }
        }
        
        public Uri PosterPath { set; get; }
    }
}
