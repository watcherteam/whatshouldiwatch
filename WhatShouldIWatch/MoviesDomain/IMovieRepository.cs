﻿using System.Collections.Generic;
using MoviesDomain;

namespace MoviesDB
{
    /// <summary>
    /// Интерфейс работы с БД
    /// </summary>
    public interface IMovieRepository
    {
        /// <summary>
        /// Извлечь из БД фильм по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Найденный фильм (если не найден, то null)</returns>
        IMovie GetMovieById(string id);

        /// <summary>
        /// Извлечь из БД фильм по названию
        /// </summary>
        /// <param name="title"></param>
        /// <returns>Найденный фильм (если не найден, то null)</returns>
        IEnumerable<IMovie> GetMovieByTitle(string title);

        /// <summary>
        /// Извлечь из БД все фильмы
        /// </summary>
        /// <returns>Все фильмы</returns>
        IEnumerable<IMovie> GetAllMovies();

        /// <summary>
        /// Сохранить фильм в БД
        /// </summary>
        /// <param name="movieToSave"></param>
        void SaveMovie(IMovie movieToSave);

        /// <summary>
        /// Удалить фильм из БД
        /// </summary>
        /// <param name="movieToDelete"></param>
        void DeleteMovie(IMovie movieToDelete);

        /// <summary>
        /// Удалить все фильмы из БД
        /// </summary>
        void DeleteAllMovies();

        /// <summary>
        /// Текущее количество фильмов в БД
        /// </summary>
        int Count { get; }
    }
}
