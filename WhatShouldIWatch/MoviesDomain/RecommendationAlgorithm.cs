﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesDomain
{
    public interface RecommendationAlgorithm
    {
        List<IMovie> MakeRecommendation(IMovie original, IEnumerable<IMovie> allMovies, int numberOfRecommendations);
    }
}
