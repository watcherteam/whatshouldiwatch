﻿using MoviesDB;
using MoviesDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhatShouldIWatch.Extensions
{
    public static class IMovieRepositorySaveAll
    {
        public static void SaveAll(this IMovieRepository repo, IEnumerable<IMovie> movies)
        {
            foreach(var movie in movies)
            {
                repo.SaveMovie(movie);
            }
        }

        public static bool IsNotEmpty(this IMovieRepository repo)
        {
            return repo.Count > 0;
        }

        public static bool IsEmpty(this IMovieRepository repo)
        {
            return repo.Count == 0;
        }
    }
}
