﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace WhatShouldIWatch.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MovieDetailsPage : Page
    {
        public MovieDetailsPage()
        {
            this.InitializeComponent();
            this.DataContext = ViewModels.VMLocator.DetailsVM;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            (this.DataContext as ViewModels.MovieDetailsPageViewModel).Movie = e.Parameter as MoviesDomain.IMovie;
        }

        private void MovieTile_Tapped(object sender, TappedRoutedEventArgs e)
        {
            (this.DataContext as ViewModels.NavigationalViewModel).NavigateToMovieDetailsPageCommand.Execute(LibraryPanel.SelectedItem);
        }
    }
}
