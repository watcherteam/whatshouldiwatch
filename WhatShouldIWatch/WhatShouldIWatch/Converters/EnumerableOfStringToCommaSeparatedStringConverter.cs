﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace WhatShouldIWatch.Converters
{
    class EnumerableOfStringToCommaSeparatedStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var data = value as IEnumerable<string>;
            

            if (parameter != null)
            {
                data = data.Take((int)parameter);
            }

            var emptyDataMsg = new Windows.ApplicationModel.Resources.ResourceLoader().GetString("TagsNotAvailiable");

            if (data == null)
            {
                return emptyDataMsg;
            }

            var sb = new StringBuilder();

            foreach(var tag in data)
            {
                sb.AppendFormat("{0}, ", tag);
            }
            if (sb.Length < 3)
            {
                return emptyDataMsg;
            }
            else
            {
                sb.Remove(sb.Length - 2, 2);
                return sb.ToString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
