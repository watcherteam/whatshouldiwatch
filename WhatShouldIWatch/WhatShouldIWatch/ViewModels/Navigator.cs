﻿using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace WhatShouldIWatch.ViewModels
{
    class Navigator : INavigationService
    {
        public string CurrentPageKey => currentPage?.FullName;

        private Type currentPage;

        public void GoBack()
        {
            AppShell shell = Window.Current.Content as AppShell;

            shell.AppFrame.GoBack();

            currentPage = shell.AppFrame.CurrentSourcePageType;
        }

        public void NavigateTo(string pageKey)
        {
            this.NavigateTo(pageKey, null);
        }

        public void NavigateTo(string pageKey, object parameter)
        {
            AppShell shell = Window.Current.Content as AppShell;

            currentPage = Type.GetType(pageKey);

            shell.AppFrame.Navigate(currentPage, parameter);
        }
    }
}
