﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight;
using MoviesDomain;

namespace WhatShouldIWatch.ViewModels
{
    public class LibraryPageViewModel : NavigationalViewModel
    {

        private List<IMovie> movies;
        public List<IMovie> Movies
        {
            get { return movies; }
            set
            {
                if (movies != value)
                {
                    movies = value;
                    RaisePropertyChanged(nameof(Movies));
                }
            }
        }


        private async void LoadTopMovies()
        {
            IsLoading = true;

            Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            int count = (int)localSettings.Values["topMoviesCount"];

            Movies = await provider.GetHighestRatedMovies(count);

            IsLoading = false;
        }

        public LibraryPageViewModel(GalaSoft.MvvmLight.Views.INavigationService navigator, IMovieProvider provider) : base(navigator, provider)
        {
            LoadTopMovies();
        }
    }
}
