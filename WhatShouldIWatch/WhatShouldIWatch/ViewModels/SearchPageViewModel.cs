﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MoviesDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhatShouldIWatch.ViewModels
{
    class SearchPageViewModel : NavigationalViewModel
    {
        private List<IMovie> movies;
        public List<IMovie> Movies
        {
            get { return movies; }
            set
            {
                if (movies != value)
                {
                    movies = value;
                    RaisePropertyChanged(nameof(Movies));
                }
            }
        }

        private string weHaveFoundMessage;
        public string WeHaveFoundMessage
        {
            get
            {
                return weHaveFoundMessage;
            }
            set
            {
                if (weHaveFoundMessage != value)
                {
                    weHaveFoundMessage = value;
                    RaisePropertyChanged(nameof(WeHaveFoundMessage));
                }
            }
        }

        public SearchPageViewModel(GalaSoft.MvvmLight.Views.INavigationService navigator, IMovieProvider provider) : base(navigator, provider)
        {
        }

        private async void SeacrhMovie(string title)
        {
            IsLoading = true;

            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

            try
            {

                Movies = await provider.SearchMovieLike(title);

                if (Movies.Count > 0)
                {
                    WeHaveFoundMessage = loader.GetString("FoundSthText");
                }
                else
                {
                    WeHaveFoundMessage = loader.GetString("FoundNothingText");
                }
            }
            catch (MovieProvider.RestStatusCodeException)
            {
                WeHaveFoundMessage = loader.GetString("FoundNothingText");
            }

            IsLoading = false;
        }



        private RelayCommand<string> searchMovieCommand;
        public RelayCommand<string> SearchMovieCommand
        {
            get
            {
                if (searchMovieCommand == null)
                {
                    searchMovieCommand = new RelayCommand<string>(SeacrhMovie);
                }
                return searchMovieCommand;
            }
        }
    }
}
