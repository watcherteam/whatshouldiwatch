﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhatShouldIWatch.ViewModels
{
    public abstract class NavigationalViewModel : ViewModelBase
    {
        private GalaSoft.MvvmLight.Views.INavigationService navigator;

        protected MoviesDomain.IMovieProvider provider;

        private void NavigateToMovieDetailsPage(Object movie)
        {
            navigator.NavigateTo(typeof(Views.MovieDetailsPage).FullName, movie);
        }

        private RelayCommand<object> navigateToMovieDetailsPageCommand;
        public RelayCommand<object> NavigateToMovieDetailsPageCommand
        {
            get
            {
                if (navigateToMovieDetailsPageCommand == null)
                {
                    navigateToMovieDetailsPageCommand = new RelayCommand<object>(NavigateToMovieDetailsPage);
                }
                return navigateToMovieDetailsPageCommand;
            }
        }

        private bool isLoading;
        public bool IsLoading
        {
            get
            {
                return isLoading;
            }
            set
            {
                if (isLoading != value)
                {
                    isLoading = value;
                    RaisePropertyChanged(nameof(IsLoading));
                }
            }
        }

        public NavigationalViewModel(GalaSoft.MvvmLight.Views.INavigationService navigator, MoviesDomain.IMovieProvider provider) : base()
        {
            this.navigator = navigator;
            this.provider = provider;
        }
    }
}
