﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MoviesDB;
using MoviesDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhatShouldIWatch.Extensions;

namespace WhatShouldIWatch.ViewModels
{
    class MovieDetailsPageViewModel : NavigationalViewModel
    {
        public MovieDetailsPageViewModel(GalaSoft.MvvmLight.Views.INavigationService navigator, IMovieProvider provider) : base(navigator, provider)
        {
            advisor = new SimilarityRecommendation();
            repo = new LiteDbContext("shittyMoves");
        }

        private MoviesDomain.IMovie movie;
        public MoviesDomain.IMovie Movie
        {
            get
            {
                return movie;
            }
            set
            {
                if (movie != value)
                {
                    movie = value;
                    RaisePropertyChanged(nameof(Movie));
                    GetRelatedMovies();
                }
            }
        }

        private List<IMovie> relatedMovies;
        public List<IMovie> RelatedMovies
        {
            get { return relatedMovies; }
            set
            {
                if (relatedMovies != value)
                {
                    relatedMovies = value;
                    RaisePropertyChanged(nameof(RelatedMovies));
                }
            }
        }


        private async void GetRelatedMovies()
        {
            IsLoading = true;
            Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            if (allMovies == null)
            {
                int count = (int)localSettings.Values["similarityTopSize"];
                if (repo.IsNotEmpty())
                {
                    allMovies = repo.GetAllMovies().ToList();
                }
                else
                {
                    allMovies = await provider.GetHighestRatedMovies(count);
                    repo.SaveAll(allMovies);
                }
            }
            int relatedCount = (int)localSettings.Values["topMoviesCount"];
            RelatedMovies = advisor.MakeRecommendation(movie, allMovies.Where(x => !x.Equals(movie)), relatedCount);
            IsLoading = false;
        }

        public int ActorsToShow => 5;

        private List<IMovie> allMovies = null;

        private SimilarityRecommendation advisor;

        IMovieRepository repo;
    }
}
