﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhatShouldIWatch.ViewModels
{
    class VMLocator
    {
        static VMLocator()
        {
            GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.Register<GalaSoft.MvvmLight.Views.INavigationService, ViewModels.Navigator>();
            GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.Register<MoviesDomain.IMovieProvider, MovieProvider.DefaultMovieProvider>();
            GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.Register<LibraryPageViewModel>();
            GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.Register<MovieDetailsPageViewModel>();
            GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.Register<SearchPageViewModel>();
        }


        public static LibraryPageViewModel LibraryVM
        {
            get
            {
                return GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<LibraryPageViewModel>();
            }
        }

        public static MovieDetailsPageViewModel DetailsVM
        {
            get
            {
                return GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<MovieDetailsPageViewModel>();
            }
        }

        public static SearchPageViewModel SearchVM
        {
            get
            {
                return GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<SearchPageViewModel>();
            }
        }
    }
}
