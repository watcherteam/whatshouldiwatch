﻿using System.Collections.Generic;
using System.Linq;
using System;
using MoviesDomain;
using LiteDB;

namespace MoviesDB
{
    /// <summary>
    /// Создание корректного пути к фйлу БД
    /// </summary>
    public static class Ext
    {
        public static string AddPathComponent(this string root, string part)
        {
            return String.Format(@"{0}\{1}", root, part);
        }
    }
    /// <summary>
    /// Реализация IMovieRepository для LiteDB
    /// </summary>
    public class LiteDbContext : IMovieRepository
    {
        public LiteDbContext(string databaseName)
        {
            if (string.IsNullOrEmpty(databaseName)) throw new ArgumentException(nameof(databaseName));

            DatabaseName = Windows.Storage.ApplicationData.Current.LocalFolder.Path.AddPathComponent(databaseName);
            using (var db = new LiteDatabase(databaseName))
            {
                db.Mapper.Entity<IMovie>().Id(x => x.MovieId, false);
                db.Mapper.Entity<MovieDescription>().Id(x => x.MovieDescriptionId, false);
                db.GetCollection<IMovie>().EnsureIndex(x => x.MovieId, true);
                db.GetCollection<MovieDescription>().EnsureIndex(x => x.MovieDescriptionId, true);
            }
        }

        public IMovie GetMovieById(string id)
        {
            if (string.IsNullOrEmpty(id)) throw new ArgumentException(nameof(id));

            using (var db = new LiteDatabase(DatabaseName))
            {
                return db.GetCollection<IMovie>().FindOne(movie => movie.MovieId == id);
            }
        }

        public IEnumerable<IMovie> GetMovieByTitle(string title)
        {
            if (string.IsNullOrEmpty(title)) throw new ArgumentException(nameof(title));

            using (var db = new LiteDatabase(DatabaseName))
            {
                return db.GetCollection<IMovie>().Find(movie => movie.Title == title).ToList();
            }
        }

        public IEnumerable<IMovie> GetAllMovies()
        {
            using (var db = new LiteDatabase(DatabaseName))
            {
                return db.GetCollection<IMovie>().FindAll().ToList();
            }
        }

        public void SaveMovie(IMovie movieToSave)
        {
            if (movieToSave == null) throw new ArgumentException(nameof(movieToSave));

            using (var db = new LiteDatabase(DatabaseName))
            {
                if (db.GetCollection<IMovie>().Exists(movie => movie.MovieId == movieToSave.MovieId))
                {
                    db.GetCollection<IMovie>().Update(movieToSave);
                }
                else
                {
                    db.GetCollection<IMovie>().Insert(movieToSave);
                }
            }
        }

        public void DeleteMovie(IMovie movieToDelete)
        {
            if (movieToDelete == null) throw new ArgumentException(nameof(movieToDelete));

            using (var db = new LiteDatabase(DatabaseName))
            {
                db.GetCollection<IMovie>().Delete(movie => movie.MovieId == movieToDelete.MovieId);
            }
        }

        public void DeleteAllMovies()
        {
            using (var db = new LiteDatabase(DatabaseName))
            {
                db.DropCollection(db.GetCollection<IMovie>().Name);
            }
        }

        public int Count
        {
            get
            {
                using (var db = new LiteDatabase(DatabaseName))
                {
                    return db.GetCollection<IMovie>().Count();
                }
            }
        }

        private static string DatabaseName { set; get; }
    }
}
