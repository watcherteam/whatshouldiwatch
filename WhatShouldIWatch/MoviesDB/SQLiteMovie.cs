﻿using System;
using System.Collections.Generic;
using SQLite.Net;
using SQLite.Net.Attributes;
using MoviesDomain;


namespace MoviesDB
{
    internal class SQLiteMovie
    {
        public SQLiteMovie(IMovie movie)
        {
            PosterPath = movie.PosterPath;
            ReleaseDate = movie.ReleaseDate;
            Tags = movie.Tags;
            Descrpiton = new SQLiteMovieDescription(movie.Descrpiton);
        }

        public IMovie ToMovie()
        {
            return new Movie(Descrpiton.ToMovieDescription(), Tags, ReleaseDate);
        }

        [PrimaryKey, AutoIncrement]
        public int Id { set; get; }

        public Uri PosterPath { set;  get; }

        public DateTime ReleaseDate { set; get; }

        public List<string> Tags { set; get; }

        public SQLiteMovieDescription Descrpiton { set; get; }
    }

    internal class SQLiteMovieDescription
    {
        public SQLiteMovieDescription(MovieDescription description)
        {
            DescriptionText = description.DescriptionText;
            Director = description.Director;
            Genre = description.Genre;
            Actors = description.Actors;
        }

        public MovieDescription ToMovieDescription()
        {
            return new MovieDescription(DescriptionText, Genre, Director, Actors);
        }

        [PrimaryKey, AutoIncrement]
        public int Id { set; get; }
        
        public string DescriptionText { set; get; }

        public string Director { set; get; }

        public List<string> Genre { set; get; }

        public List<string> Actors { set; get; }
    }
}
