﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MovieProvider
{
    public class RestHttpClient : IHttpClient
    {
        public RestHttpClient()
        {
            client = new HttpClient();
        }

        public Task<HttpResponseMessage> GetAsync(string query, CancellationToken token = new CancellationToken())
        {
            return client.GetAsync(query, token);
        }

        public void Dispose()
        {
            client.Dispose();
        }

        private readonly HttpClient client;
    }
}
