﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MovieProvider.TmdbResponses
{
    [DataContract]
    internal class Genre
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
    }

    [DataContract]
    internal class GenresResponse
    {
        [DataMember(Name = "genres")]
        public List<Genre> Genres { get; set; }
    }
}