﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MovieProvider.TmdbResponses
{
    /// <summary>
    /// Ответ на запрос /movie/{movie_id}/keywords
    /// Содержит список тэгов фильма
    /// </summary>
    [DataContract]
    internal class Keyword
    {
        /// <summary>
        /// Id тэга
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Название тэга
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }
    }

    [DataContract]
    internal class KeywordsResponse
    {
        /// <summary>
        /// Id фильма
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Тэги фильма
        /// </summary>
        [DataMember(Name = "keywords")]
        public List<Keyword> Keywords { get; set; }
    }
}