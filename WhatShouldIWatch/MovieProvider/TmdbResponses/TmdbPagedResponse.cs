﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MovieProvider.TmdbResponses
{
    /// <summary>
    /// Ответ на запрос, возвращаемый странично (/Discover, /Search)
    /// </summary>
    /// <typeparam name="T">Результат запроса</typeparam>
    [DataContract]
    internal class TmdbPagedResponse<T>
    {
        [DataMember(Name = "page")]
        public int Page { get; set; }

        [DataMember(Name = "total_pages")]
        public int TotalPages { get; set; }

        [DataMember(Name = "total_results")]
        public int TotalResults { get; set; }

        [DataMember(Name = "results")]
        public List<T> Results { get; set; }
    }
}
