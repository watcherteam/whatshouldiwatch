﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MovieProvider.TmdbResponses
{

    [DataContract]
    internal class Actor
    {
        [DataMember(Name = "character")]
        public string Character { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
    }

    [DataContract]
    internal class CrewMember
    {
        [DataMember(Name = "job")]
        public string Job { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
    }

    [DataContract]
    internal class CreditResponse
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "cast")]
        public List<Actor> Cast { get; set; }

        [DataMember(Name = "crew")]
        public List<CrewMember> Crew { get; set; }
    }
}