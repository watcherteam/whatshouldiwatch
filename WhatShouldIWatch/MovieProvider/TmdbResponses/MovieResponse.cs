﻿using MoviesDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MovieProvider.TmdbResponses
{
    /// <summary>
    /// Ответ на постраничные запросы /discover/movie и /search/movie
    /// </summary>
    [DataContract]
    internal class MovieResponse
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "poster_path")]
        public string PosterPath { get; set; }

        [DataMember(Name = "overview")]
        public string Overview { get; set; }

        [DataMember(Name = "release_date")]
        public string ReleaseDate { get; set; }

        [DataMember(Name = "genre_ids")]
        public List<int> GenreList { get; set; }
    }
}
