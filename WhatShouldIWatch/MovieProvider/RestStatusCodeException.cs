﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MovieProvider
{
    public class RestStatusCodeException : System.Exception
    {
        public RestStatusCodeException(HttpStatusCode statusCode) 
            : base($"Service returned bad status code: {statusCode}")
        {
            StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; set; }
    }
}
