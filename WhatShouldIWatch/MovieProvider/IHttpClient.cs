﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MovieProvider
{
    public interface IHttpClient : IDisposable
    {
        Task<HttpResponseMessage> GetAsync(string query, CancellationToken token = default(CancellationToken));
    }
}
