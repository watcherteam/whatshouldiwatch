﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MovieProvider.TmdbResponses;
using MoviesDomain;
using PortableRest;

namespace MovieProvider
{
    /// <summary>
    /// Источник фильмов из TMDB
    /// </summary>
    public class TmdbMovieProvider : IMovieProvider
    {
        /// <summary>
        /// Конструктор TmdbMovieProvider
        /// </summary>
        /// <param name="context">Контекст запроса</param>
        /// <param name="apiKey">Ключ для доступа к TMDB</param>
        /// <param name="baseImageUrl">Адрес картинок</param>
        /// <exception cref="System.ArgumentNullException">Кидается, если apiKey==null</exception>
        public TmdbMovieProvider(IRestContext context, string apiKey, string baseImageUrl)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (apiKey == null) throw new ArgumentNullException(nameof(apiKey));

            this.context = context;
            this.apiKey = apiKey;
            this.baseImageUrl = new Uri(baseImageUrl);
        }

        public async Task<List<IMovie>> GetHighestRatedMovies(int numberOfMovies, CancellationToken token)
        {
            await FillGenresIfNeeded(token).ConfigureAwait(false);

            int remainingMovies = numberOfMovies;
            var result = new List<IMovie>();
            int pageNumber = 1;
            int totalPages = 1;

            while (remainingMovies > 0 && pageNumber <= totalPages)
            {
                FillContext(@"discover/movie");
                context.AddQueryString("sort_by", "popularity.desc");
                context.AddQueryString("page", $"{pageNumber}");

                try
                {
                    var answer =
                        await context.ExecuteAsync<TmdbPagedResponse<MovieResponse>>(token).ConfigureAwait(false);
                    var answerMovies = new List<IMovie>();
                    foreach (var movieResponse in answer.Results.Take(Math.Min(remainingMovies, answer.Results.Count)))
                    {
                        IMovie m = await MovieResponseToMovie(movieResponse, token);
                        answerMovies.Add(m);
                    }

                    result.AddRange(answerMovies);

                    remainingMovies -= answer.Results.Count;
                    pageNumber++;
                    totalPages = answer.TotalPages;
                }
                catch (RestStatusCodeException e)
                {
                    if (e.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        throw new InvalidApiKeyException(apiKey);
                    }
                    throw;
                }
            }

            return result;
        }

        public async Task<List<IMovie>> SearchMovieLike(string name, CancellationToken token)
        {
            await FillGenresIfNeeded(token).ConfigureAwait(false);

            var result = new List<IMovie>();
            int pageNumber = 1;
            int totalPages = 1;

            while (pageNumber <= totalPages)
            {
                FillContext(@"search/movie");
                context.AddQueryString("page", $"{pageNumber}");
                context.AddQueryString("query", $"{name}");

                try
                {
                    var answer =
                        await context.ExecuteAsync<TmdbPagedResponse<MovieResponse>>(token).ConfigureAwait(false);

                    var answerMovies = new List<IMovie>();
                    foreach (var movieResponse in answer.Results)
                    {
                        IMovie m = await MovieResponseToMovie(movieResponse, token);
                        answerMovies.Add(m);
                    }

                    result.AddRange(answerMovies);

                    pageNumber++;
                    totalPages = answer.TotalPages;
                }
                catch (RestStatusCodeException e)
                {
                    if (e.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        throw new InvalidApiKeyException(apiKey);
                    }
                    throw;
                }
            }

            return result;
        }

        private void FillContext(string resource)
        {
            context.StartNewQuery(resource);
            context.AddQueryString("api_key", apiKey);
            context.AddQueryString("language", "ru-RU");
        }

        private async Task<IMovie> MovieResponseToMovie(MovieResponse response, CancellationToken token)
        {
            var credits = await GetCredits(response.Id, token).ConfigureAwait(false);
            var keywords = await GetKeywords(response.Id, token).ConfigureAwait(false);
            var genres = response.GenreList?.Where(x => genreTable.ContainsKey(x)).Select(x => genreTable[x]);
            var descr = MakeDescription(response.Overview, genres, credits);

            DateTime date;
            DateTime.TryParse(response.ReleaseDate, out date);

            Uri poster = null;
            if (!string.IsNullOrEmpty(response.PosterPath))
            {
                poster = new Uri(baseImageUrl + response.PosterPath.TrimStart('/'));
            }

            return new Movie(response.Title, descr, keywords.Keywords.Select(x => x.Name),
                    date, poster);
        }

        private async Task FillGenresIfNeeded(CancellationToken token)
        {
            if (genreTable != null)
            {
                return;
            }

            FillContext("genre/movie/list");

            var genresAnswer = await context.ExecuteAsync<GenresResponse>(token).ConfigureAwait(false);

            genreTable = new Dictionary<int, string>();
            foreach (var g in genresAnswer.Genres)
            {
                genreTable.Add(g.Id, g.Name);
            }
        }

        private async Task<CreditResponse> GetCredits(int movieId, CancellationToken token)
        {
            FillContext($@"movie/{movieId}/credits");

            return await context.ExecuteAsync<CreditResponse>(token).ConfigureAwait(false);
        }

        private async Task<KeywordsResponse> GetKeywords(int movieId, CancellationToken token)
        {
            FillContext($@"movie/{movieId}/keywords");

            return await context.ExecuteAsync<KeywordsResponse>(token).ConfigureAwait(false);
        }

        private MovieDescription MakeDescription(string overview, IEnumerable<string> genres, CreditResponse credit)
        {
            var director = credit?.Crew.Where(x => x.Job.ToLower() == "director").Select(x => x.Name)
                .FirstOrDefault() ?? "N/A";
            var cast = credit?.Cast.Select(x => x.Name).Distinct();
            return new MovieDescription(overview, genres, director, cast);
        }

        private Dictionary<int, string> genreTable;

        private readonly IRestContext context;
        private readonly string apiKey;

        private readonly Uri baseImageUrl;
        public void Dispose()
        {
            context?.Dispose();
        }
    }
}