﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MovieProvider
{
    /// <summary>
    /// Контекст для REST сервисов
    /// </summary>
    public interface IRestContext : IDisposable
    {
        /// <summary>
        /// Начать новый запрос
        /// </summary>
        void StartNewQuery(string resource);

        /// <summary>
        /// Добавить пару ключ-значение в запрос
        /// </summary>
        void AddQueryString(string key, string value);

        /// <summary>
        /// Выполнить REST запрос
        /// </summary>
        /// <typeparam name="T">Тип результата</typeparam>
        /// <returns>Task&lt;T&gt; на результат запроса</returns>
        /// <exception cref="RestStatusCodeException">Кидается, если сервис возвращает
        /// ошибочный статус возврата</exception>
        Task<T> ExecuteAsync<T>(CancellationToken token = default(CancellationToken))
            where T: class;
    }
}