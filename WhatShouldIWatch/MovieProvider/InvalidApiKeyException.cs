﻿namespace MovieProvider
{
    public class InvalidApiKeyException : System.Exception
    {
        public InvalidApiKeyException(string apiKey) : base($"Key:{apiKey} is invalid") { }
    }
}