﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MoviesDomain;

namespace MovieProvider
{
    public class DefaultMovieProvider : IMovieProvider
    {
        public DefaultMovieProvider()
        {
            provider = new TmdbMovieProvider(new RestContext(new RestHttpClient(), 
                new Uri("http://api.themoviedb.org/3/")),
                "441c593dc94848db4296e46edac5ef17", @"http://image.tmdb.org/t/p/w640/");
        }

        public void Dispose()
        {
            provider.Dispose();
        }

        public Task<List<IMovie>> GetHighestRatedMovies(int numberOfMovies, CancellationToken token = new CancellationToken())
        {
            return provider.GetHighestRatedMovies(numberOfMovies, token);
        }

        public Task<List<IMovie>> SearchMovieLike(string name, CancellationToken token = new CancellationToken())
        {
            return provider.SearchMovieLike(name, token);
        }

        private readonly IMovieProvider provider;
    }
}