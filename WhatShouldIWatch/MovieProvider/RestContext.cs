﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Newtonsoft.Json;
using PortableRest;

namespace MovieProvider
{
    public class RestContext : IRestContext
    {
        public RestContext(IHttpClient client, Uri baseUrl)
        {
            if (baseUrl == null) throw new ArgumentNullException(nameof(baseUrl));

            this.client = client;
            this.baseUrl = baseUrl;
            queryParameters = new List<KeyValuePair<string, string>>();
        }

        public void StartNewQuery(string resource)
        {
            this.resource = resource;
            queryParameters.Clear();
        }

        public void AddQueryString(string key, string value)
        {
            queryParameters.Add(new KeyValuePair<string, string>(key, WebUtility.UrlEncode(value)));
        }

        public async Task<T> ExecuteAsync<T>(CancellationToken token = default(CancellationToken))
            where T : class
        {
            var fullQuery = baseUrl + resource + ParameterString;
            HttpResponseMessage response = await client.GetAsync(fullQuery, token).ConfigureAwait(false);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return JsonConvert.DeserializeObject<T>(content);
            }

            token.ThrowIfCancellationRequested();

            // 429 Too many requests
            // Попытаемся снова
            if (response.StatusCode == (HttpStatusCode) 429)
            {
                return await RetryWithDelay<T>(response.Headers.RetryAfter.Delta.GetValueOrDefault(), token).ConfigureAwait(false);
            }
            throw new RestStatusCodeException(response.StatusCode);
        }

        private async Task<T> RetryWithDelay<T>(TimeSpan delay, CancellationToken token) where T : class
        {
            await Task.Delay(delay, token);
            return await ExecuteAsync<T>(token).ConfigureAwait(false);
        }

        private string ParameterString
        {
            get { return queryParameters.Aggregate("?", (acc, x) => acc + $"{x.Key}={x.Value}&").TrimEnd('&'); }
        }

        private readonly List<KeyValuePair<string, string>> queryParameters;
        private readonly IHttpClient client;
        private readonly Uri baseUrl;
        private string resource;

        public void Dispose()
        {
            client?.Dispose();
        }
    }
}