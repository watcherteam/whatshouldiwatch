﻿using System;
using MoviesDomain;
using Xunit;

namespace MoviesDomainTest
{
    public class MovieTest
    {
        [Fact]
        public void TitleYearReturnsCorrectString()
        {
            Movie m = new Movie("A title",
                new MovieDescription("director", new[]{""}, "", new []{""}),
                new []{""},
                new DateTime(1994, 4, 2), null);

            Assert.True(m.TitleYear == "A title (1994)");
        }
    }
}