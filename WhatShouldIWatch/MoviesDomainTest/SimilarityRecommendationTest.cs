﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoviesDomain;
using Xunit;

namespace MoviesDomainTest
{
    public class UnitTest1
    {

        public Movie MakeMovie(params string[] tags)
        {
            MovieDescription descr = new MovieDescription("", new[] {""}, "", new[] {""});

            return new Movie("title", descr, tags, new DateTime(), null);
        }

        [Fact]
        public void SimilarityRecommendationShouldReturnRequestedNumberOfMovies()
        {
            List<Movie> movies = new List<Movie>
            {
                MakeMovie("tag"),
                MakeMovie("tag"),
                MakeMovie("tag"),
                MakeMovie("tag"),
                MakeMovie("tag"),
            };

            Movie m = MakeMovie("tag");

            SimilarityRecommendation similarity = new SimilarityRecommendation();

            Assert.Equal(similarity.MakeRecommendation(m, movies, 0).Count, 0);
            Assert.Equal(similarity.MakeRecommendation(m, movies, 3).Count, 3);
        }

        [Fact]
        public void SimilarityRecommendationShouldCorrectlyRecommendMovieWithoutRecommendations()
        {
            List<Movie> movies = new List<Movie>
            {
                MakeMovie("tag"),
                MakeMovie("tag"),
                MakeMovie("tag"),
                MakeMovie("tag"),
                MakeMovie("tag"),
            };

            Movie m = MakeMovie();

            SimilarityRecommendation similarity = new SimilarityRecommendation();

            
            Assert.Equal(similarity.MakeRecommendation(m, movies, 0).Count, 0);
            Assert.Equal(similarity.MakeRecommendation(m, movies, 3).Count, 3);
        }

        [Fact]
        public void SimilarityRecommendationShouldRecommendMostSimilarMovies()
        {
            Movie m = MakeMovie("a", "b", "c", "d", "othertag");
            Movie mostSimilarMovie = MakeMovie("a", "b", "c", "d", "e"); // 4/5 тэгов совпадают
            Movie secondMostSimilarMovie = MakeMovie("a", "b", "c", "z"); // 3/5 тэгов совпадают
            Movie thirdMostSimilarMovie = MakeMovie("a", "z"); // 1/5 тэгов совпадают

            List<Movie> movies = new List<Movie>
            {
                MakeMovie("tag"),
                secondMostSimilarMovie,
                thirdMostSimilarMovie,
                MakeMovie("tag"),
                mostSimilarMovie,
                MakeMovie("tag")
            };

            SimilarityRecommendation similarity = new SimilarityRecommendation();
            var recommendedMovies = similarity.MakeRecommendation(m, movies, 3);

            Assert.True(recommendedMovies.SequenceEqual(new [] {mostSimilarMovie, secondMostSimilarMovie, thirdMostSimilarMovie}));
        }
    }
}
