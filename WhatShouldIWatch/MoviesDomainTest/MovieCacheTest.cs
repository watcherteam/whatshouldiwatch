﻿using System;
using MoviesDomain;
using Xunit;

namespace MoviesDomainTest
{
    public class MovieCacheTest
    {
        [Fact]
        public void MovieShouldBeAddedToCache()
        {
            MovieCache cache = new MovieCache(1);

            Movie first = new Movie("A title",
                new MovieDescription("director", new[] { "" }, "", new[] { "" }),
                new[] { "" },
                new DateTime(1994, 4, 2), null);

            cache.AddMovie(first);

            Assert.True(cache.GetMovie(first.MovieId) != null);
        }

        [Fact]
        public void MovieShouldReplaceOldCacheEntry()
        {
            MovieCache cache = new MovieCache(1);

            Movie first = new Movie("A title",
                new MovieDescription("director", new[] { "" }, "", new[] { "" }),
                new[] { "" },
                new DateTime(1994, 4, 2), null);
            Movie second = new Movie("A title 2",
                new MovieDescription("director", new[] { "" }, "", new[] { "" }),
                new[] { "" },
                new DateTime(1994, 4, 2), null);

            cache.AddMovie(first);
            cache.AddMovie(second);

            Assert.True((cache.GetMovie(second.MovieId) != null) &&
                (cache.GetMovie(first.MovieId) == null));
        }
    }
}
