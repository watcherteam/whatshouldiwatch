﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoviesDomain;
using MoviesDB;
using Xunit;


namespace MoviesDBTest
{
    public class DBContextTest
    {
        IMovie CreateMovie(string title, string testString)
        {
            return new Movie(title, new MovieDescription(testString, new string[] { testString }, testString, new string[] { testString }),
                new string[] { testString }, DateTime.Now, new Uri("https://"+testString+".io"));
        }

        [Fact]
        void ConnectionToDatabaseShouldBeEstablished()
        {
            // Не упало, значит подсоединилось
            LiteDbContext context = new LiteDbContext(@"test.db");
        }

        [Fact]
        void AllMoviesShouldBeDeleted()
        {
            LiteDbContext context = new LiteDbContext(@"test.db");
            
            IMovie[] movies = new IMovie[]  { CreateMovie("aaa", "a1"),
                                              CreateMovie("aab", "a2"),
                                              CreateMovie("aba", "a3") };

            foreach (var movie in movies)
            {
                context.SaveMovie(movie);
            }

            context.DeleteAllMovies();

            Assert.Equal(0, context.Count);
        }

        [Fact]
        void MovieShouldBeDeleted()
        {
            LiteDbContext context = new LiteDbContext(@"test.db");

            IMovie[] movies = new IMovie[]  { CreateMovie("aaa", "a1"),
                                              CreateMovie("aab", "a2"),
                                              CreateMovie("aba", "a3") };
            IMovie movieToDelete = movies.First();

            foreach (var movie in movies)
            {
                context.SaveMovie(movie);
            }

            context.DeleteMovie(movieToDelete);

            Assert.Equal(movies.Count()-1, context.Count);
            Assert.Null(context.GetMovieById(movieToDelete.MovieId));
        }

        [Fact]
        void MoviesShouldBeInserted()
        {
            LiteDbContext context = new LiteDbContext(@"test.db");
            context.DeleteAllMovies();

            IMovie[] movies = new IMovie[]  { CreateMovie("aaa", "a1"),
                                              CreateMovie("aab", "a2"),
                                              CreateMovie("aba", "a3") };

            foreach (var movie in movies)
            {
                context.SaveMovie(movie);
            }

            Assert.Equal(movies.Count(), context.Count);
        }

        [Fact]
        void MovieShouldBeInserted()
        {
            LiteDbContext context = new LiteDbContext(@"test.db");
            context.DeleteAllMovies();

            IMovie movie = CreateMovie("aaa", "a1");

            context.SaveMovie(movie);
            var result = context.GetMovieById(movie.MovieId);
            Assert.True(movie.Equals(result));
        }

        [Fact]
        void MoviesShouldBeUpdated()
        {
            LiteDbContext context = new LiteDbContext(@"test.db");
            context.DeleteAllMovies();

            IMovie[] movies = new IMovie[]  { CreateMovie("aaa", "a1"),
                                                  CreateMovie("aab", "a2"),
                                                  CreateMovie("aba", "a3") };
            IMovie movieToUpdate = movies.First();

            foreach (var movie in movies)
            {
                context.SaveMovie(movie);
            }

            context.SaveMovie(movieToUpdate);

            var result = context.GetAllMovies().ToArray();
            Assert.Equal(movies.Count(), context.Count);
        }

        [Fact]
        void MovieShouldBeUpdated()
        {
            LiteDbContext context = new LiteDbContext(@"test.db");
            context.DeleteAllMovies();

            IMovie movie = CreateMovie("aaa", "a1");
            context.SaveMovie(movie);

            movie.Title = "Awesome Film";
            movie.Tags.Add("It's just soooo awesome!!!");
            context.SaveMovie(movie);

            var result = context.GetMovieById(movie.MovieId);
            Assert.True(movie.Equals(result));
        }
    }
}
