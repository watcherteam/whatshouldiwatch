﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Etg.SimpleStubs;
using MovieProvider;
using MovieProvider.TmdbResponses;
using Xunit;

namespace MovieProviderTest
{
    public class RestContextTest
    {
        [Fact]
        public void ContextWillRetryOnTooManyRequests()
        {
            // Arrange

            // Библиотека не поддерживает MustHaveHappened и подобные,
            // поэтому сделаем переменную, показывающую, что тест зашёл во второй вызов
            bool hitRetry = false;
            var stub = new StubIHttpClient()
                .GetAsync(async (query, token) =>
                {
                    var response = new HttpResponseMessage((HttpStatusCode) 429);
                    response.Headers.RetryAfter = new RetryConditionHeaderValue(new TimeSpan(0));
                    return await Task.Run(() => response);
                }, Times.Once)
                .GetAsync(async (query, token) =>
                {
                    hitRetry = true;
                    var response = new HttpResponseMessage(HttpStatusCode.OK);
                    response.Content = new StringContent(@"{}");
                    return await Task.Run(() => response);
                }, Times.Once);
            var context = new RestContext(stub, new Uri("http://localhost"));

            // Act
            context.ExecuteAsync<KeywordsResponse>().ConfigureAwait(false).GetAwaiter().GetResult();

            // Assert
            Assert.True(hitRetry);
        }

        [Fact]
        public void ContextWillThrowOnWrongStatus()
        {
            // Arrange
            var stub = new StubIHttpClient()
                .GetAsync(async (query, token) =>
                {
                    var response = new HttpResponseMessage((HttpStatusCode) 404);
                    return await Task.Run(() => response);
                }, Times.Once);
            var context = new RestContext(stub, new Uri("http://localhost"));

            // Act/Assert
            Assert.Throws<RestStatusCodeException>(
                () => context.ExecuteAsync<KeywordsResponse>().ConfigureAwait(false).GetAwaiter().GetResult());
        }

        [Fact]
        public void ContextWillCorrectlyDeserializeGoodData()
        {
            // Arrange
            var stub = new StubIHttpClient()
                .GetAsync(async (query, token) =>
                {
                    var response = new HttpResponseMessage((HttpStatusCode)HttpStatusCode.OK);
                    response.Content = new StringContent(@"{""id"" : 3417, ""keywords"":[{""id"":1008,""name"":""guerrilla""}]}");
                    return await Task.Run(() => response);
                }, Times.Once);
            var context = new RestContext(stub, new Uri("http://localhost"));

            // Act
            var keywordsResponse = context.ExecuteAsync<KeywordsResponse>().ConfigureAwait(false).GetAwaiter().GetResult();
            Assert.Equal(keywordsResponse.Id, 3417);

            var keywords = keywordsResponse.Keywords;
            Assert.Equal(keywords.Count, 1);
            Assert.Equal(keywords[0].Id, 1008);
            Assert.Equal(keywords[0].Name, "guerrilla");
        }
    }
}
