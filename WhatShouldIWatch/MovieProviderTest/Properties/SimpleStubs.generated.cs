using System;
using System.Runtime.CompilerServices;
using Etg.SimpleStubs;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MoviesDomain;

namespace MovieProvider
{
    [CompilerGenerated]
    public class StubIHttpClient : IHttpClient
    {
        private readonly StubContainer<StubIHttpClient> _stubs = new StubContainer<StubIHttpClient>();

        global::System.Threading.Tasks.Task<global::System.Net.Http.HttpResponseMessage> global::MovieProvider.IHttpClient.GetAsync(string query, global::System.Threading.CancellationToken token)
        {
            return _stubs.GetMethodStub<GetAsync_String_CancellationTokenCancellationToken_Delegate>("GetAsync").Invoke(query, token);
        }

        public delegate global::System.Threading.Tasks.Task<global::System.Net.Http.HttpResponseMessage> GetAsync_String_CancellationTokenCancellationToken_Delegate(string query, global::System.Threading.CancellationToken token);

        public StubIHttpClient GetAsync(GetAsync_String_CancellationTokenCancellationToken_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        void global::System.IDisposable.Dispose()
        {
            _stubs.GetMethodStub<IDisposable_Dispose_Delegate>("Dispose").Invoke();
        }

        public delegate void IDisposable_Dispose_Delegate();

        public StubIHttpClient Dispose(IDisposable_Dispose_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }
    }
}

namespace MovieProvider
{
    [CompilerGenerated]
    public class StubIRestContext : IRestContext
    {
        private readonly StubContainer<StubIRestContext> _stubs = new StubContainer<StubIRestContext>();

        void global::MovieProvider.IRestContext.StartNewQuery(string resource)
        {
            _stubs.GetMethodStub<StartNewQuery_String_Delegate>("StartNewQuery").Invoke(resource);
        }

        public delegate void StartNewQuery_String_Delegate(string resource);

        public StubIRestContext StartNewQuery(StartNewQuery_String_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        void global::MovieProvider.IRestContext.AddQueryString(string key, string value)
        {
            _stubs.GetMethodStub<AddQueryString_String_String_Delegate>("AddQueryString").Invoke(key, value);
        }

        public delegate void AddQueryString_String_String_Delegate(string key, string value);

        public StubIRestContext AddQueryString(AddQueryString_String_String_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        global::System.Threading.Tasks.Task<T> global::MovieProvider.IRestContext.ExecuteAsync<T>(global::System.Threading.CancellationToken token)
        {
            return _stubs.GetMethodStub<ExecuteAsync_CancellationTokenCancellationToken_Delegate<T>>("ExecuteAsync<T>").Invoke(token);
        }

        public delegate global::System.Threading.Tasks.Task<T> ExecuteAsync_CancellationTokenCancellationToken_Delegate<T>(global::System.Threading.CancellationToken token) where T : class;

        public StubIRestContext ExecuteAsync<T>(ExecuteAsync_CancellationTokenCancellationToken_Delegate<T> del, int count = Times.Forever, bool overwrite = false) where T : class
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        void global::System.IDisposable.Dispose()
        {
            _stubs.GetMethodStub<IDisposable_Dispose_Delegate>("Dispose").Invoke();
        }

        public delegate void IDisposable_Dispose_Delegate();

        public StubIRestContext Dispose(IDisposable_Dispose_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }
    }
}

namespace MoviesDomain
{
    [CompilerGenerated]
    public class StubIMovie : IMovie
    {
        private readonly StubContainer<StubIMovie> _stubs = new StubContainer<StubIMovie>();

        string global::MoviesDomain.IMovie.MovieId
        {
            get
            {
                return _stubs.GetMethodStub<MovieId_Get_Delegate>("get_MovieId").Invoke();
            }

            set
            {
                _stubs.GetMethodStub<MovieId_Set_Delegate>("set_MovieId").Invoke(value);
            }
        }

        string global::MoviesDomain.IMovie.Title
        {
            get
            {
                return _stubs.GetMethodStub<Title_Get_Delegate>("get_Title").Invoke();
            }

            set
            {
                _stubs.GetMethodStub<Title_Set_Delegate>("set_Title").Invoke(value);
            }
        }

        global::System.Uri global::MoviesDomain.IMovie.PosterPath
        {
            get
            {
                return _stubs.GetMethodStub<PosterPath_Get_Delegate>("get_PosterPath").Invoke();
            }

            set
            {
                _stubs.GetMethodStub<PosterPath_Set_Delegate>("set_PosterPath").Invoke(value);
            }
        }

        global::System.DateTime global::MoviesDomain.IMovie.ReleaseDate
        {
            get
            {
                return _stubs.GetMethodStub<ReleaseDate_Get_Delegate>("get_ReleaseDate").Invoke();
            }

            set
            {
                _stubs.GetMethodStub<ReleaseDate_Set_Delegate>("set_ReleaseDate").Invoke(value);
            }
        }

        global::System.Collections.Generic.List<string> global::MoviesDomain.IMovie.Tags
        {
            get
            {
                return _stubs.GetMethodStub<Tags_Get_Delegate>("get_Tags").Invoke();
            }

            set
            {
                _stubs.GetMethodStub<Tags_Set_Delegate>("set_Tags").Invoke(value);
            }
        }

        global::MoviesDomain.MovieDescription global::MoviesDomain.IMovie.Description
        {
            get
            {
                return _stubs.GetMethodStub<Description_Get_Delegate>("get_Description").Invoke();
            }

            set
            {
                _stubs.GetMethodStub<Description_Set_Delegate>("set_Description").Invoke(value);
            }
        }

        public delegate void MovieId_Set_Delegate(string value);

        public StubIMovie MovieId_Set(MovieId_Set_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        public delegate string MovieId_Get_Delegate();

        public StubIMovie MovieId_Get(MovieId_Get_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        public delegate void Title_Set_Delegate(string value);

        public StubIMovie Title_Set(Title_Set_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        public delegate string Title_Get_Delegate();

        public StubIMovie Title_Get(Title_Get_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        public delegate void PosterPath_Set_Delegate(global::System.Uri value);

        public StubIMovie PosterPath_Set(PosterPath_Set_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        public delegate global::System.Uri PosterPath_Get_Delegate();

        public StubIMovie PosterPath_Get(PosterPath_Get_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        public delegate void ReleaseDate_Set_Delegate(global::System.DateTime value);

        public StubIMovie ReleaseDate_Set(ReleaseDate_Set_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        public delegate global::System.DateTime ReleaseDate_Get_Delegate();

        public StubIMovie ReleaseDate_Get(ReleaseDate_Get_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        public delegate void Tags_Set_Delegate(global::System.Collections.Generic.List<string> value);

        public StubIMovie Tags_Set(Tags_Set_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        public delegate global::System.Collections.Generic.List<string> Tags_Get_Delegate();

        public StubIMovie Tags_Get(Tags_Get_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        public delegate void Description_Set_Delegate(global::MoviesDomain.MovieDescription value);

        public StubIMovie Description_Set(Description_Set_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        public delegate global::MoviesDomain.MovieDescription Description_Get_Delegate();

        public StubIMovie Description_Get(Description_Get_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        bool global::System.IEquatable<global::MoviesDomain.IMovie>.Equals(global::MoviesDomain.IMovie other)
        {
            return _stubs.GetMethodStub<IEquatableOfIMovie_Equals_IMovie_Delegate>("Equals").Invoke(other);
        }

        public delegate bool IEquatableOfIMovie_Equals_IMovie_Delegate(global::MoviesDomain.IMovie other);

        public StubIMovie Equals(IEquatableOfIMovie_Equals_IMovie_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }
    }
}

namespace MoviesDomain
{
    [CompilerGenerated]
    public class StubIMovieProvider : IMovieProvider
    {
        private readonly StubContainer<StubIMovieProvider> _stubs = new StubContainer<StubIMovieProvider>();

        global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::MoviesDomain.Movie>> global::MoviesDomain.IMovieProvider.GetHighestRatedMovies(int numberOfMovies, global::System.Threading.CancellationToken token)
        {
            return _stubs.GetMethodStub<GetHighestRatedMovies_Int32_CancellationTokenCancellationToken_Delegate>("GetHighestRatedMovies").Invoke(numberOfMovies, token);
        }

        public delegate global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::MoviesDomain.Movie>> GetHighestRatedMovies_Int32_CancellationTokenCancellationToken_Delegate(int numberOfMovies, global::System.Threading.CancellationToken token);

        public StubIMovieProvider GetHighestRatedMovies(GetHighestRatedMovies_Int32_CancellationTokenCancellationToken_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::MoviesDomain.Movie>> global::MoviesDomain.IMovieProvider.SearchMovieLike(string name, global::System.Threading.CancellationToken token)
        {
            return _stubs.GetMethodStub<SearchMovieLike_String_CancellationTokenCancellationToken_Delegate>("SearchMovieLike").Invoke(name, token);
        }

        public delegate global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::MoviesDomain.Movie>> SearchMovieLike_String_CancellationTokenCancellationToken_Delegate(string name, global::System.Threading.CancellationToken token);

        public StubIMovieProvider SearchMovieLike(SearchMovieLike_String_CancellationTokenCancellationToken_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        void global::System.IDisposable.Dispose()
        {
            _stubs.GetMethodStub<IDisposable_Dispose_Delegate>("Dispose").Invoke();
        }

        public delegate void IDisposable_Dispose_Delegate();

        public StubIMovieProvider Dispose(IDisposable_Dispose_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }
    }
}

namespace MoviesDB
{
    [CompilerGenerated]
    public class StubMovieRepository : MovieRepository
    {
        private readonly StubContainer<StubMovieRepository> _stubs = new StubContainer<StubMovieRepository>();

        int global::MoviesDB.MovieRepository.Count
        {
            get
            {
                return _stubs.GetMethodStub<Count_Get_Delegate>("get_Count").Invoke();
            }
        }

        global::MoviesDomain.IMovie global::MoviesDB.MovieRepository.GetMovieById(string id)
        {
            return _stubs.GetMethodStub<GetMovieById_String_Delegate>("GetMovieById").Invoke(id);
        }

        public delegate global::MoviesDomain.IMovie GetMovieById_String_Delegate(string id);

        public StubMovieRepository GetMovieById(GetMovieById_String_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        global::System.Collections.Generic.IEnumerable<global::MoviesDomain.IMovie> global::MoviesDB.MovieRepository.GetMovieByTitle(string title)
        {
            return _stubs.GetMethodStub<GetMovieByTitle_String_Delegate>("GetMovieByTitle").Invoke(title);
        }

        public delegate global::System.Collections.Generic.IEnumerable<global::MoviesDomain.IMovie> GetMovieByTitle_String_Delegate(string title);

        public StubMovieRepository GetMovieByTitle(GetMovieByTitle_String_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        global::System.Collections.Generic.IEnumerable<global::MoviesDomain.IMovie> global::MoviesDB.MovieRepository.GetAllMovies()
        {
            return _stubs.GetMethodStub<GetAllMovies_Delegate>("GetAllMovies").Invoke();
        }

        public delegate global::System.Collections.Generic.IEnumerable<global::MoviesDomain.IMovie> GetAllMovies_Delegate();

        public StubMovieRepository GetAllMovies(GetAllMovies_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        void global::MoviesDB.MovieRepository.SaveMovie(global::MoviesDomain.IMovie movieToSave)
        {
            _stubs.GetMethodStub<SaveMovie_IMovie_Delegate>("SaveMovie").Invoke(movieToSave);
        }

        public delegate void SaveMovie_IMovie_Delegate(global::MoviesDomain.IMovie movieToSave);

        public StubMovieRepository SaveMovie(SaveMovie_IMovie_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        void global::MoviesDB.MovieRepository.DeleteMovie(global::MoviesDomain.IMovie movieToDelete)
        {
            _stubs.GetMethodStub<DeleteMovie_IMovie_Delegate>("DeleteMovie").Invoke(movieToDelete);
        }

        public delegate void DeleteMovie_IMovie_Delegate(global::MoviesDomain.IMovie movieToDelete);

        public StubMovieRepository DeleteMovie(DeleteMovie_IMovie_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        void global::MoviesDB.MovieRepository.DeleteAllMovies()
        {
            _stubs.GetMethodStub<DeleteAllMovies_Delegate>("DeleteAllMovies").Invoke();
        }

        public delegate void DeleteAllMovies_Delegate();

        public StubMovieRepository DeleteAllMovies(DeleteAllMovies_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }

        public delegate int Count_Get_Delegate();

        public StubMovieRepository Count_Get(Count_Get_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }
    }
}

namespace MoviesDomain
{
    [CompilerGenerated]
    public class StubRecommendationAlgorithm : RecommendationAlgorithm
    {
        private readonly StubContainer<StubRecommendationAlgorithm> _stubs = new StubContainer<StubRecommendationAlgorithm>();

        global::System.Collections.Generic.List<global::MoviesDomain.Movie> global::MoviesDomain.RecommendationAlgorithm.MakeRecommendation(global::MoviesDomain.Movie original, global::System.Collections.Generic.IEnumerable<global::MoviesDomain.Movie> allMovies, int numberOfRecommendations)
        {
            return _stubs.GetMethodStub<MakeRecommendation_Movie_IEnumerableOfMovie_Int32_Delegate>("MakeRecommendation").Invoke(original, allMovies, numberOfRecommendations);
        }

        public delegate global::System.Collections.Generic.List<global::MoviesDomain.Movie> MakeRecommendation_Movie_IEnumerableOfMovie_Int32_Delegate(global::MoviesDomain.Movie original, global::System.Collections.Generic.IEnumerable<global::MoviesDomain.Movie> allMovies, int numberOfRecommendations);

        public StubRecommendationAlgorithm MakeRecommendation(MakeRecommendation_Movie_IEnumerableOfMovie_Int32_Delegate del, int count = Times.Forever, bool overwrite = false)
        {
            _stubs.SetMethodStub(del, count, overwrite);
            return this;
        }
    }
}